project(libunity-2d-private)

set(libunity-2d-private_SOVERSION 0)
set(libunity-2d-private_VERSION ${libunity-2d-private_SOVERSION}.0.0)

add_subdirectory(src)
add_subdirectory(Unity2d)
add_subdirectory(tests)
