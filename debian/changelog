unity-2d (3.8.10-1harjit0) bionic; urgency=low

  * Dolorem Ipsum:
    - trying to port to Bionic.

 -- HarJIT <harjit@harjit.moe>  Sat, 08 Feb 2020 01:08:00 +0000

unity-2d (3.8.10-0ubuntu2) oneiric; urgency=low

  * debian/control:
    - rebuild with new indicator 0.4
  * 01_build_with_new_indicator.patch:
    - patch to build with the new indicator 0.4 stack

 -- Didier Roche <didrocks@ubuntu.com>  Sun, 10 Jul 2011 23:48:01 +0200

unity-2d (3.8.10-0ubuntu1) oneiric; urgency=low

  [ Florian Boucault ]
  * debian/unity-2d-launcher.install:
    - do not install launcher/launchermenu.css anylonger as its now shipped as
      part as libunity-2d-private-qml.so using the Qt resource system
  * debian/libunity-2d-private0:
    - move symbolic link to /usr/share/backgrounds/warty-final-ubuntu.png from
      /usr/share/unity-2d/places/artwork to /usr/share/unity-2d 
  * debian/control:
    - add pkg-config as a build dependency 

  [ Didier Roche ]
  * New upstream release:
    - FTBFS due to erroneous check on libwnck (LP: #800550)
    - [dash] Search is slow and freezes (LP: #774569)
    - Some community lenses do not display anything - UnityHorizontalRenderer
      not implemented (LP: #790319)
    - [underthehood] the ListAggregatorModel class is not unit tested
      (LP: #794482)
    - Missing a GConf schema file for the /desktop/unity-2d/… keys
      (LP: #750303)
    - [dash] home screen search hint should be 'Search' (LP: #797825)
  * debian/unity-2d.gconf-defaults,
    debian/20_ubuntu-2d-gconf-default:
    - transition some gconf keys from session-wide to system-wide
      (LP: #797672, #797676)
  * debian/control:
    - ensure we are still using gtk2 indicator for alpha2 (LP: #804938)
    - bump standards-version

 -- Didier Roche <didrocks@ubuntu.com>  Mon, 04 Jul 2011 11:05:54 +0200

unity-2d (3.8.8-0ubuntu1) oneiric; urgency=low

  [ Didier Roche ]
  * new upstream release:
    - [spread] Corruption when switching workspaces after windows have been
      moved to other workspaces (LP: #760787)
    - [launcher] launcher won't fully paint, corrupted view (LP: #764690)
    - [launcher] icons no longer active after an incomplete drag (LP: #768812)
    - drag from dash to launcher (LP: #662616)
    - Don't create windows over the launcher (LP: #688816)
    - [launcher] Does not reveal when hovering over the left edge of the
      screen (LP: #760537)
    - UIFe: Launcher - update launcher reveal interaction to make it more
      accessible to first time users (LP: #754583)
    - [launcher] Trash icon should indicate when it has deleted elements
      (LP: #715611)
    - When windows open for the first time they should not hide the launcher
      (LP: #723878)
    - double clicks should be disabled on bfb/Place launcher icon/double key
      press (LP: #766776)
    - [dash][launcher] Should use real transparency when a compositing manager
      is running (LP: #794042)
    - [dash] Thunderbird icon is pixelated (LP: #767115)
    - [panel] Hovering the mouse cursor over the BFB reveals the current
      window’s menu (LP: #793403)
    - [panel] Hovering the mouse cursor away from the appmenu applet doesn’t
      hide the menu (LP: #793406)
    - unity-2d: does not parse QT_GRAPHICSSYSTEM env var (LP: #791852)
    - Cannot drag applications from dash to desktop (LP: #756614)
  * debian/control:
    - unity-2d-panel recommends the indicator, not unity-2d
    - appmenu-gtk and appmenu-qt are already provided by indicator-appmenu
    - remove other meaningless recommends
  * debian/libunity-2d-private0.install:
    - install everything in Unity2d private directory

  [ Florian Boucault ]
  * debian/unity-2d-launcher.install:
    - do not install usr/lib/qt4/imports/UnityApplications/ anylonger as all of
      UnityApplications features have been moved to the Unity2d QML plugin
      installed by libunity-2d-private0
  * debian/control:
    - unity-2d-places and unity-2d-spread do not depend on unity-2d-launcher.
      All they need is now in libunity-2d-private0.

 -- Didier Roche <didrocks@ubuntu.com>  Tue, 14 Jun 2011 16:14:18 +0200

unity-2d (3.8.6-0ubuntu3~ppa1) oneiric; urgency=low

  * Rebuild with new gcc

 -- Didier Roche <didrocks@ubuntu.com>  Tue, 07 Jun 2011 15:00:32 +0200

unity-2d (3.8.6-0ubuntu2) oneiric; urgency=low

  * debian/unity-2d.postinst, debian/unity-2d.postrm:
    - remove the magic to set unity-2d in oneiric as the default session in gdm
      as it's now installed on the ubuntu CD.

 -- Didier Roche <didrocks@ubuntu.com>  Wed, 01 Jun 2011 09:42:21 +0200

unity-2d (3.8.6-0ubuntu1) oneiric; urgency=low

  * New upstream release:
    - [launcher] Support static shortcuts in the quicklists. (LP: #669923)
    - [launcher] Make sure the whole area of the item tile handle mouse events
      (LP: #743402)
    - [dash, launcher, spread] Added a new command line switch (-opengl) that
      triggers the use of a QGLWidget for the QML viewport. (LP: #773397)
    - [launcher] Fix the last shortcut text to display "0", not "10", to be
      consistent with a typical keyboard layout. (LP: #743420)
    - [launcher] Keyboard shortcuts for place entries (LP: #732637)
    - <Meta>+T to open the trash. (LP: #771886)
    - [launcher] Use appropriate icon for devices depending on their type
      (instead of always showing a USB icon). (LP: #703309)
    - [launcher] Grab the Meta+n (for 0 ≤ n ≤ 9) hotkeys at startup, and release
      them only when exiting. (LP: #758650)
    - Fix the byte order of images on Big Endian architectures (like PowerPC).
      Original algorithm by Paul J. Wells, reworked for efficiency.
      (LP: #758782)
    - [launcher] Ensure all delegates are cached in order to improve smoothness
      of scrolling on very low end platforms. (LP: #780566)
    - [dash] Shortcuts expanded state are now persistent. (LP: #774437)
    - [launcher] Match trash nautilus window with the trash item in the
      launcher. (LP: #692444)
    - [launcher] For the workspaces and window overviews, trigger Compiz's Expo
      and Scale plugins if Compiz is running. If Compiz isn't running, said
      plugins aren't enabled or the D-Bus plugin isn't enabled, fall back to
      using unity-2d-spread. (LP: #760674, #715244)
  * debian/control:
    - remove deprecated transitional packages (only from maverick ppa or early
      natty alpha)
    - don't dep on gnome-session-bin
    - remove the -dev package, uneeded for a private library internal to this
      source
    - remove bzr build-dep, we will refresh the pot with dh_translations
  * debian/rules:
    - build with dh7 now
    - buid with --fail-missing and purging installed but not shipped private
      headers
  * debian/unity-2d-launcher.preinst:
    - removed, deprecated as it was for the ppa migration
  * debian/libunity-2d-private0.post*
    - removed: ldconfig is generated directly by dh_makeshlibs as we don't pass
      -n anymore
  * don't install unity-2d session as it's in gnome-session now
  * debian/unity-2d.install
    debian/20_ubuntu-2d-gconf-default
    debian/20_ubuntu-2d-gconf-mandatory
    debian/gconf/ubuntu-2d.default.path
    debian/gconf/ubuntu-2d.mandatory.path:
    - remove uneeded defaults and mandatory
    - remove some deprecated keys
    - the session is now called ubuntu-2d and not unity-2d anymore
  * debian/unity-2d.preinst, debian/unity-2d.postinst
    debian/unity-2d.postrm:
    - take into account the natty -> oneiric migration with session name change

 -- Didier Roche <didrocks@ubuntu.com>  Wed, 25 May 2011 12:19:25 +0200

unity-2d (3.8.4.1-0ubuntu1) natty; urgency=low

  * (LP: #766078) [spread] Windows and background are not positioned correctly

 -- Oliver Grawert <ogra@ubuntu.com>  Tue, 19 Apr 2011 20:01:06 +0200

unity-2d (3.8.4-0ubuntu1) natty; urgency=low

  * New upstream bugfix release
    * (LP: #764690)  [launcher] launcher won't fully paint, corrupted view
    * (LP: #740387)  graphical corruption with multiple drivers and classic desktop
    * (LP: #753269)  [spread] crash when sending applications to other workspaces
    * (LP: #755599)  [dash] Exiting the dash causes launcher and panel to disappear
    * (LP: #760522)  [launcher] LibreOffice applications that are not favorite are all
      matched with LibreOffice Impress
    * (LP: #763116)  [launcher] Launcher doesn’t autohide after an application
      requested attention
    * (LP: #684471)  [dash] Default terminal PATH when opening it from the Applications
      menu is / instead of $HOME
    * (LP: #739454)  [launcher] nautilus windows are not matched with the home icon
    * (LP: #741160)  [launcher] Launching nautilus changes the favorite’s icon
    * (LP: #744970)  [launcher] item calling for attention needs to always be visible
    * (LP: #751450)  [launcher] icon for workspace switcher incorrect (back to old pink
      one)
    * (LP: #753382)  [launcher] Synaptic favorite turns into software-properties
    * (LP: #759189)  [dash] no singular/plural option for "See %1 more results"
    * (LP: #759776)  [launcher] Looses track of open windows for synaptic and empathy
    * (LP: #728927)  Unity Min,Max,Close buttons not available in Libreoffice
    * (LP: #759191)  [launcher] most keyboard shortcuts don't activate properly the
      first time/ever
    * (LP: #760001)  [launcher] Super key shouldn't activate dash while spread is
      (LP: #running
    * (LP: #760233)  [launcher] critical warning from wnck
    * (LP: #761661)  [spread] New WindowInfo objects should not be created while spread
      is inactive
    * (LP: #721121)  UIFe: Icon in Launcher should be home folder icon

 -- Oliver Grawert <ogra@ubuntu.com>  Tue, 19 Apr 2011 14:23:17 +0200

unity-2d (3.8.2-0ubuntu1) natty; urgency=low

  [ Oliver Grawert ]
  * New upstream bugfix release
   - (LP: #632526)  Dash elipsizes file and application names too soon, making them
     unreadable
   - (LP: #669926) [launcher] Web favorites support
   - (LP: #708479) Dash view should use "Prefferred Applications" icons where
     appropriate
   - (LP: #718686) [dash] Group of results sometimes badly positioned
   - (LP: #727483) unity-2d-panel crashed with SIGSEGV in g_return_if_fail_warning()
   - (LP: #731449) [launcher] Dragging a tile at the top of the launcher while
     autoscrolling makes autoscroll wrong afterwards
   - (LP: #736097) [dash] home screen misses icons for applications that are not
     installed
   - (LP: #744999) [launcher] launchers are truncated when too many items to fit
     onscreen
   - (LP: #745077) [spread] clicking launcher with open windows not working correctly
     across workspaces
   - (LP: #745237) [dash] search field default string not translated
   - (LP: #746693) [launcher] .places messages not i18nized
   - (LP: #747836) [dash] Banshee no longer works from the dash home page in 3.8.2
   - (LP: #750753) [dash] showing/hiding places causing graphical corruption
   - (LP: #670608) [dash] Home screen customization should be easy
   - (LP: #683084) Global menu doesn't work well with more than one screen
   - (LP: #714646) [launcher] icons jagged edges during animation
   - (LP: #717744) [panel] inactive menus are clickable
   - (LP: #729002) First four items in Dash begin "Find" "Find" "Find" "Find"
   - (LP: #745758) [spread] super+s should toggle the workspace switcher
   - (LP: #751284) [launcher] Escaping of title broken with webfavorites
   - (LP: #751325) [panel] circle of friends button icon needs to be updated to match
     Unity's
   - (LP: #697816) [launcher] if an urgent window is available then the spread should
     not be activated
   - (LP: #729478) [launcher] Clicking middle mouse button should launch another
     instance of application
   - (LP: #750244) [launcher] Newly installed lenses don’t appear
   - (LP: #752948) Home's "Shortcuts" not i18n/l10n

  [ Aurélien Gâteau ]
  * Include .mo files in unity-2d package (LP: #751425)

 -- Oliver Grawert <ogra@ubuntu.com>  Fri, 08 Apr 2011 16:03:10 +0200

unity-2d (3.8.1-0ubuntu1) natty; urgency=low

  * Upstream bugfix to fix global search showing no results (LP: #741201)

 -- Oliver Grawert <ogra@ubuntu.com>  Wed, 23 Mar 2011 20:52:29 +0100

unity-2d (3.8-0ubuntu1) natty; urgency=low

  * New upstream bugfix release
   - fixes (LP: #580295),(LP: #670403),(LP: #676457),(LP: #706247),
   (LP: #708429),(LP: #719507),(LP: #724717),(LP: #726712),(LP: #726715),
   (LP: #726727),(LP: #727164),(LP: #727409),(LP: #730499),(LP: #733244),
   (LP: #733960),(LP: #738025),(LP: #739417),(LP: #740137),(LP: #740280),
   (LP: #691114),(LP: #701543),(LP: #701546),(LP: #703389),(LP: #703396),
   (LP: #705642),(LP: #706248),(LP: #706713),(LP: #709280),(LP: #711081),
   (LP: #714707),(LP: #716167),(LP: #721049),(LP: #722711),(LP: #722713),
   (LP: #723604),(LP: #726630),(LP: #726682),(LP: #726716),(LP: #727883),
   (LP: #729699),(LP: #730638),(LP: #730880),(LP: #732978),(LP: #733154),
   (LP: #733897),(LP: #733959),(LP: #734143),(LP: #738332),(LP: #683026),
   (LP: #718866),(LP: #721121),(LP: #724316),(LP: #731165),(LP: #734074),
   (LP: #735500),(LP: #716506),(LP: #733150),(LP: #672447),(LP: #716580),
   (LP: #728291),(LP: #730884),(LP: #731263),(LP: #731266)

 -- Oliver Grawert <ogra@ubuntu.com>  Wed, 23 Mar 2011 15:31:16 +0100

unity-2d (3.6.2-0ubuntu2) natty; urgency=low

  * do not run pkgbinarymangler to convert images to 8bit since this breaks
    the launcher icon background handling
    (QT upstream bug: http://bugreports.qt.nokia.com/browse/QTBUG-4459)

 -- Oliver Grawert <ogra@ubuntu.com>  Fri, 18 Mar 2011 13:08:56 +0100

unity-2d (3.6.2-0ubuntu1) natty; urgency=low

  * New upstream bugfix release

 -- Oliver Grawert <ogra@ubuntu.com>  Tue, 15 Mar 2011 17:59:55 +0100

unity-2d (3.6.1-0ubuntu1) natty; urgency=low

  * New Upstream, fix FTBFS with po files generation

 -- Oliver Grawert <ogra@ubuntu.com>  Thu, 24 Feb 2011 15:19:02 +0100

unity-2d (3.6.0-0ubuntu1) natty; urgency=low

  * New Upstream version

 -- Oliver Grawert <ogra@ubuntu.com>  Thu, 24 Feb 2011 13:45:27 +0100

unity-2d (3.2.2-0ubuntu1) natty; urgency=low

  * new upstream version

 -- Oliver Grawert <ogra@ubuntu.com>  Fri, 28 Jan 2011 15:41:48 +0100

unity-2d (3.2.1-0ubuntu1) natty; urgency=low

  * new upstream version, we have minor upstream versions now

 -- Oliver Grawert <ogra@ubuntu.com>  Thu, 27 Jan 2011 15:00:23 +0100

unity-2d (3.2-0ubuntu3) natty; urgency=low

  * Conflicts/Replaces needs to be Breaks/Replaces

 -- Oliver Grawert <ogra@ubuntu.com>  Wed, 26 Jan 2011 20:36:09 +0100

unity-2d (3.2-0ubuntu2) natty; urgency=low

  * merge unity-2d-default-settings package as new unity-2d toplevel 
    package.
  * fix various typos in package descriptions (LP: #703170)

 -- Oliver Grawert <ogra@ubuntu.com>  Wed, 26 Jan 2011 18:27:04 +0100

unity-2d (3.2-0ubuntu1) natty; urgency=low

  * switch to proper upstream versioning, we are actually at 3.2 not 1.0

 -- Oliver Grawert <ogra@ubuntu.com>  Fri, 21 Jan 2011 14:39:01 +0100

unity-2d (0.1-0ubuntu4) natty; urgency=low

  * add Vcs-Bzr location to debian/control
  * update branch location in debian/copyright
  * pull in some upstream fixes

 -- Oliver Grawert <ogra@ubuntu.com>  Wed, 21 Jan 2011 13:11:45 +0100

unity-2d (0.1-0ubuntu3) natty; urgency=low

  * drop the unity-place-files and unity-place-applications recommends and 
    dependencies as these are uninstallable in natty right now

 -- Oliver Grawert <ogra@ubuntu.com>  Mon, 17 Jan 2011 15:28:20 +0100

unity-2d (0.1-0ubuntu2) natty; urgency=low

  * Fix libuqpanel.symbols for platform independence

 -- Jonathan Riddell <jriddell@ubuntu.com>  Sat, 15 Jan 2011 02:27:19 +0000

unity-2d (0.1-0ubuntu1) natty; urgency=low

  * Initial Release (LP: #703156)

 -- Michael Casadevall <mcasadevall@ubuntu.com>  Fri, 14 Jan 2011 17:42:08 -0800
