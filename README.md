Trying to get Unity 2D running on modern Ubuntu.

This is a very old version of Unity 2D (3.8.10), since later versions have more complicated 
compatibility problems. I've backported a couple minor points / fixes from version 5.14.0.

See debian/control for dependencies. libutouch-geis-dev is now libgeis-dev.

You'll probably want to `gconftool -t bool -s /desktop/unity-2d/launcher/use_strut true` to stop
windows from dropping behind the launcher when maximised. Alternatively, if using Openbox, just 
edit `~/.config/openbox/lxde-rc.xml` to hardcode the maximisation region (or do the same via the 
Openbox config GUI), but ideally just set `use_strut`.

No, the applications menu doesn't work for some reason. The wallpaper is disabled (I'm using
PCManFM anyway, which manages its own wallpaper, so trying to port that QML to whatever the gconf
bindings are doing nowadays wasn't worth the effort). The rest works. Sort of.

For removing the rendundent title bar from maximised windows, see:
https://dglava.github.io/en/orcsome.html (itself needs: `apt install libev-dev`,
`pip2 install orcsome`). Or in Openbox, just right-click and "Un/Decorate".

If the home button is displaying a cropped oversized Ubuntu logo on your Ubuntu version, the 
following should resolve that (unless you prefer it like that, of course):

~~~ bash
sudo ln -s /usr/share/icons/unity-icon-theme/places/24/distributor-logo.png /usr/share/icons/hicolor/24x24/apps/distributor-logo.png
sudo gtk-update-icon-cache /usr/share/icons/hicolor
~~~

Original README:

--------------------------

Compiling unity-2d
==================

    cmake . && make


Running separate components
===========================

    ./launcher/app/unity-2d-launcher
    ./panel/app/unity-2d-panel
    ./places/app/unity-2d-places


Notes on the dash (places)
==========================

The dash exposes a D-Bus property for activation and deactivation:

    com.canonical.Unity2d /Dash com.canonical.Unity2d.Dash.active true|false

